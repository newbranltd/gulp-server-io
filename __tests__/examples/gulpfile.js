/* eslint-ignore */
'use strict';
/**
 * gulp example setup
 */
const { join } = require('path');
const gulp = require('gulp');
const gulpServerIo = require('../../index');
const root = [
  join(__dirname, '..', 'fixtures', 'app'),
  join(__dirname, '..', 'fixtures', 'rootDir')
];

const devDir = join(__dirname, '..', '.tmp');

const config1 = {
  port: 3456
}; // add stuff here

  //////////////////////////////
  //      TEST INJECTION      //
  //////////////////////////////

const config2 = {
  inject: {
    enable: true,
    source: [
      'css/bootstrap.min.css',
      'css/starter-template.css',
      'js/bootstrap.min.js',
      'js/ie10-viewport-bug-workaround.js'
    ]
    // target: ['another.html']
  }
  /*
  serverReload: {
    dir: path.join(__dirname, '..', '.tmp'),
    callback: files => {
      console.log('detect files change', files);
    }
  }*/
};

gulp.task('copy', () => gulp.src(join(root[0], '**', '*'))
  .pipe(gulp.dest(devDir))
);

gulp.task('serve', () => gulp.src(root)
  .pipe(gulpServerIo(config1))
);

gulp.task('serveDev', () => gulp.src(devDir)
  .pipe(gulpServerIo(config2))
);

gulp.task('watch', done => {
  gulp.watch(join(root[0], '**', '*'), gulp.series('copy'));
  done();
});

// testing the wiredep
gulp.task('wire', () => {
  return gulp.src(root)
    .pipe(
      gulpServerIo(config2)
    )
});

gulp.task('dev', gulp.series('copy', 'serveDev', 'watch'));
