/**
 * Test against a basic setup
 */
import test from 'ava';
import request from 'supertest';
import { join } from 'path';
import webserver from '../../index';
import {
  rootDir,
  defaultUrl
} from '../fixtures/config.js';

let stream;

const _config = {debugger: false, reload: false};

test.beforeEach(() => {
  stream = webserver(_config);
  stream.write(rootDir);
});

test.afterEach(() => {
  stream.emit('kill');
  stream = undefined;
});

test(`(1) Basic test - it should work with default options ${defaultUrl}`, async t => {
  t.plan(1);
  const res = await request(defaultUrl)
    .get('/');

  t.is(res.status, 200);
});
