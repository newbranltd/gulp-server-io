/**
 * Test against the standalone server
 */
import test from 'ava';
import request from 'supertest';
import { join } from 'path';
import webserver from '../../server';
import {
  root,
  defaultUrl
} from '../fixtures/config.js';

let stream;

const _config = {
  webroot: root
};

test.beforeEach(() => {
  stream = webserver(_config);
});

test.afterEach(() => {
  stream.close();
  stream = undefined;
});

test(`(1) Basic test - it should work with default options ${defaultUrl}`, async t => {
  t.plan(1);
  const res = await request(defaultUrl)
    .get('/');

  t.is(res.status, 200);
});
